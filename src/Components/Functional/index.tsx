import { useState } from "react";
import { UrlInput } from "./UrlInput";

export function FunctionalForm() {
    const [url, setUrl] = useState("https://catfact.ninja/fact");
    const [res, setRes] = useState("");
    const [hasError, setHasError] = useState(false);

    const responseClass = hasError ? "errored" : "";

    return <div><h2>Компоненты на функциях</h2>
        <UrlInput setUrl={setUrl} url={url} setRes={setRes} setHasError={setHasError}></UrlInput>
        <div className={responseClass}><p>{res}</p></div>
    </div>
}
