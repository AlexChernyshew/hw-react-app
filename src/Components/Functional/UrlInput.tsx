import axios from "axios";
import { UrlInputProps } from "../../types";

export function UrlInput(props: UrlInputProps) {

    const handleUpdateUrl = (event: React.FormEvent<HTMLInputElement>) => {
        props.setUrl(event.currentTarget.value)
    }

    const sendRequest = async () => {
        try {
            const res = await axios.get(props.url);
            props.setRes(JSON.stringify(res.data));
            props.setHasError(false);
        }
        catch (err) {
            props.setHasError(true);
            props.setRes(JSON.stringify(err));
        }
    }

    return <div>
        <input type="url" value={props.url} onInput={handleUpdateUrl} />
        <button onClick={sendRequest}>Отправить</button>
    </div>
}