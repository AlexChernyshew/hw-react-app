import React, { ReactNode, Component } from 'react';
import UrlInput from './UrlInput';

export default class ClassesForm extends Component {
    state = {
        url: "https://catfact.ninja/fact",
        response: "",
        hasError: false
    }

    setHasError = (hasError: boolean) => {
        this.setState((state) => ({
            ...state,
            hasError: hasError
        }))
    }
    setUrl = (text: string) => {
        this.setState((state) => ({
            ...state,
            url: text
        }))
    }

    setRes = (text: string) => {
        console.log(text);
        this.setState((state) => ({
            ...state,
            response: text
        }))
    }

    get responseClass() {
        return this.state.hasError ? "errored" : "";
    }

    render() {
        return <div><h2>Компоненты на классах</h2>
            <UrlInput setHasError={this.setHasError} setUrl={this.setUrl} url={this.state.url} setRes={this.setRes}></UrlInput>
            <div className={this.responseClass}><p>{this.state.response}</p></div>
        </div>;
    }
}