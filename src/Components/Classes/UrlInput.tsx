import axios from "axios";
import { Component, ReactNode } from "react";
import { UrlInputProps } from "../../types";

export default class UrlInput extends Component<UrlInputProps> {
    sendRequest = async () => {
        try {
            console.log(this.props.url);
            const res = await axios.get(this.props.url);
            this.props.setRes(JSON.stringify(res.data));
            this.props.setHasError(false);
        }
        catch (err) {
            this.props.setHasError(true);
            this.props.setRes(JSON.stringify(err));
        }
    }


    handleUpdateUrl = (event: React.FormEvent<HTMLInputElement>) => {
        this.props.setUrl(event.currentTarget.value)
    }

    render(): ReactNode {
        return <div>
            <input type="url" value={this.props.url} onInput={this.handleUpdateUrl} />
            <button onClick={this.sendRequest}>Отправить</button>
        </div>
    }
}
