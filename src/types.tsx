export interface ColorButtonProps {
    onClick: (text: string) => void;
    text: string;
}

export interface BlockProps {
    text: string;
}

export interface UrlInputProps {
    url: string;
    setUrl: (text: string) => void;
    setRes: (text: string) => void;
    setHasError: (hasError: boolean) => void;
}