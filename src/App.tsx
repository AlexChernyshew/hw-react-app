import './App.css';
import { FunctionalForm } from './Components/Functional';
import ClassesForm from './Components/Classes';

function App() {
  return (
    <div>
      <ClassesForm></ClassesForm>
      <FunctionalForm></FunctionalForm>
    </div>
  );
}

export default App;